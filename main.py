import pymongo

client = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = client["PPAP_OEE"]

mycol = mydb["production_orders"]

for i in mycol.find():
    print(i)

print("*******************************************")
li=[]
for i in mycol.find({"DATE":{"$gte":"24-Sep-2020"}}):

    li.append(i)
    print(i)

if not li:
    print("NO RECORDS FOUND for query GET PRODUCTION ORDERS AFTER 24 September")
else:
    print("RECORDS FOUND for GET PRODUCTION ORDERS AFTER 24 September")

print("*******************************************")

lis=[]
for i in mycol.find({"DATE":"23-Sep-2020" or "24-Sep-2020"}):

    lis.append(i)
    print(i)

if not lis:
    print("NO RECORDS FOUND for query RECORDS FOUND for GET SHIFT RECORDS WITH SHIFT START DATE as 23 September or 24 Sept")
else:
    print("RECORDS FOUND for query GET SHIFT RECORDS WITH SHIFT START DATE as 23 September or 24 Sept")
